import 'package:flutter/material.dart';
import 'package:shop_519h0077/constants.dart';
import 'package:shop_519h0077/models/Product.dart';
import 'package:shop_519h0077/screens/details/detail_screen.dart';
import 'categories.dart';
import 'item_card.dart';

class Body extends StatelessWidget {
  const Body({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: defalutPadding),
          child: Text(
            "Rev Zone",
            style: Theme.of(context)
                .textTheme
                .headline5
                ?.copyWith(fontWeight: FontWeight.bold),
          ),
        ),
        Categories(),
        Expanded(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: defalutPadding),
              child: ListView.builder(
                  padding: EdgeInsets.all(defalutPadding / 2),
                  itemCount: products.length,
                  itemBuilder: (context, index) =>
                    ItemCard(
                      product: products[index],
                      press: () => Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) =>
                              DetailsScreen(product: products[index]),
                          ),
                      ),
                    ),
              ),
            ),
        ),
      ],
    );
  }
}