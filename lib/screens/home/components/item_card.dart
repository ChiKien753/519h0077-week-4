import 'package:flutter/material.dart';
import 'package:shop_519h0077/constants.dart';
import 'package:shop_519h0077/models/Product.dart';

class ItemCard extends StatelessWidget {
  final Product product;
  final VoidCallback press;
  const ItemCard({
    Key? key,
    required this.product,
    required this.press
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: press,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(defalutPadding),
            // height: 180,
            // width: 160,
            decoration: BoxDecoration(
              color: product.color,
              borderRadius: BorderRadius.circular(20.0),
            ),
            child: Hero(tag: '${product.id}',child: Image.asset(product.image)),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: defalutPadding / 4),
            child: Text(
              product.title,
              style: TextStyle(color: txtColor, fontWeight: FontWeight.bold, fontSize: 20.0),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: defalutPadding / 2),
            child: Text("\$${product.price}", style: TextStyle(fontSize: 15.0),),
          )
        ],
      ),
    );
  }
}