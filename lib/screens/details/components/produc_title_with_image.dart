import 'package:flutter/material.dart';
import 'package:shop_519h0077/constants.dart';
import 'package:shop_519h0077/models/Product.dart';

class ProductTitleWithImage extends StatelessWidget {
  const ProductTitleWithImage({
    Key? key,
    required this.product,
  }) : super(key: key);

  final Product product;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: defalutPadding),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            "Sportbike",
            style: TextStyle(
              color: txtColor,
              fontWeight: FontWeight.bold,
              fontSize: 16,
            ),
          ),
          Text(
            product.title,
            style: Theme.of(context).textTheme.headline4?.copyWith(
                color: txtColor, fontWeight: FontWeight.bold),
          ),
          SizedBox(height: defalutPadding,),
          Row(
            children: <Widget>[
              RichText(
                text: TextSpan(
                  children: [
                    TextSpan(text: "Price\n", style: TextStyle(color: txtColor)),
                    TextSpan(
                        text: "\$${product.price}",
                        style: Theme.of(context)
                          .textTheme
                          .headline4
                          ?.copyWith(
                            color: txtColor,
                            fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(width: defalutPadding,),
              Expanded(
                child: Hero(
                  tag: "${product.id}",
                  child: Image.asset(
                    product.image,
                    fit: BoxFit.fill
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}