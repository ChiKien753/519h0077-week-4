import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shop_519h0077/constants.dart';

import 'cart_counter.dart';

class CounterWithLikeBtn extends StatelessWidget {
  const CounterWithLikeBtn({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        CartCounter(),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: defalutPadding / 2),
          child: Container(
            padding: EdgeInsets.all(8),
            height: 32,
            width: 32,
            decoration: BoxDecoration(
              color: Color(0xfffff6464),
              shape: BoxShape.circle,
            ),
            child: SvgPicture.asset("assets/icons/heart.svg"),
          ),
        ),
      ],
    );
  }
}

