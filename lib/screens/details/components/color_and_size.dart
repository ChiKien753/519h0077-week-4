import 'package:flutter/material.dart';
import 'package:shop_519h0077/constants.dart';
import 'package:shop_519h0077/models/Product.dart';

class ColorAndSize extends StatelessWidget {
  const ColorAndSize({
    Key? key,
    required this.product,
  }) : super(key: key);

  final Product product;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                "Color",
              ),
              Row(
                children: <Widget>[
                  ColorDot(
                    color: Color(0xff47ae3c),
                    isSelected: true,
                  ),
                  ColorDot(
                    color: Color(0xff333333),
                  ),
                  ColorDot(
                    color: Color(0xff1818d7),
                  ),
                ],
              ),
            ],
          ),
        ),
        Expanded(
          child: Stack(
            children: [
              RichText(
                text: TextSpan(
                  style: TextStyle(color: txtColor),
                  children: [
                    TextSpan(text: 'Engine\n'),
                    TextSpan(
                      text: '${product.size} cm',
                      style: Theme.of(context).textTheme.headline5?.copyWith(fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              ),
              Positioned(
                child: Text(
                  '3',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
                top: 10,
                left: 80,
              )
            ],
          ),
        )
      ],
    );
  }
}

class ColorDot extends StatelessWidget {
  final Color color;
  final bool isSelected;
  const ColorDot({Key? key, required this.color, this.isSelected = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin:
          EdgeInsets.only(top: defalutPadding / 4, right: defalutPadding / 2),
      padding: EdgeInsets.all(2.5),
      height: 24,
      width: 24,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        border: Border.all(
          color: isSelected ? color : Colors.transparent,
        ),
      ),
      child: DecoratedBox(
        decoration: BoxDecoration(
          color: color,
          shape: BoxShape.circle,
        ),
      ),
    );
  }
}
