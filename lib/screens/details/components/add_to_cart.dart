import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:shop_519h0077/constants.dart';
import 'package:shop_519h0077/models/Product.dart';

class AddToCart extends StatelessWidget {
  const AddToCart({
    Key? key,
    required this.product,
  }) : super(key: key);

  final Product product;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: defalutPadding),
      child: Row(
        children: [
          Container(
            margin: EdgeInsets.only(right: defalutPadding),
            height: 50,
            width: 60,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(16),
              border: Border.all(color: txtColor),
            ),
            child: IconButton(
              icon: SvgPicture.asset(
                "assets/icons/cart.svg",
                color: txtColor,
              ),
              onPressed: () {},
            ),
          ),
          Expanded(
            child: SizedBox(
              height: 50,
              child: FlatButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(25),
                ),
                color: product.color,
                onPressed: () {},
                child: Text(
                  "Buy Now",
                  style: TextStyle(
                    fontSize: 17,
                    fontWeight: FontWeight.bold,
                    color: txtColor,
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}

