import 'package:flutter/material.dart';

class Product {
  final String image, title, description;
  final int price, size, id;
  final Color color;

  Product({required this.image, required this.title, required this.description, required this.price, required this.size,
      required this.id, required this.color}); 
}

List<Product> products = [
  Product(
    id: 1, 
    title: "Zx-10r", 
    price: 23000, 
    size: 999, 
    description: 'This is Zx-10r', 
    image: "assets/images/zx10r.png",
    color: const Color(0xffdbfabb)
  ),

  Product(
    id: 2, 
    title: "R1", 
    price: 25000, 
    size: 999, 
    description: 'This is R1', 
    image: "assets/images/r1.jpg",
    color: const Color(0xffe4f5fe)
  ),

  Product(
    id: 3, 
    title: "CBR1000RR-R", 
    price: 30000, 
    size: 999, 
    description: 'This is CBR1000RR-R', 
    image: "assets/images/cbr1000rrr.png",
    color: const Color(0xffffcccb)
  ),

  Product(
    id: 4, 
    title: "Hayabusa", 
    price: 30000, 
    size: 1340, 
    description: 'This is Hayabusa', 
    image: "assets/images/hayabusa.png",
    color: const Color(0xffd9d9d6)
  ),

  Product(
    id: 5, 
    title: "S1000rr", 
    price: 26000, 
    size: 999, 
    description: 'This is S1000rr', 
    image: "assets/images/s1000rr.png",
    color: const Color(0xffffcccb)
  ),
];